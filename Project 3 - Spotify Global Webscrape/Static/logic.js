// Create a map object
var myMap = L.map("map", {
  center: [0, 0],
  zoom: 2
});

var currentMarkers = [];

// Add a tile layer
L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}", {
  attribution: "Map data &copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a> contributors, <a href=\"https://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, Imagery © <a href=\"https://www.mapbox.com/\">Mapbox</a>",
  maxZoom: 18,
  id: "mapbox.streets",
  accessToken: API_KEY
}).addTo(myMap);

function removeAllMarkers() {
  for (var i = 0; i < currentMarkers.length; ++i) {
    const marker = currentMarkers[i];
    marker.remove();
  }
  currentMarkers = [];
}

function showTopSongsPerCountry(){
  removeAllMarkers();
  // fetch data from flask api
  fetch('http://localhost:5000/topSongPerCountryFromDb')
    .then(data => data.json())
    .then(data => {
      const items = data.items;
      for (var i = 0; i < items.length; i++) {
        var item = items[i];
        const marker = L.marker(item.Location)
          .bindPopup("<h1>" + item.Country + "</h1> <hr> <h3>Most Streamed Song is " + item.Song + "</h3>")
          .addTo(myMap);
        currentMarkers.push(marker);
      }
    })
    .catch(e => console.log('error fetching data'));

}

function showTopArtistPerCountry(){
  removeAllMarkers();
  // fetch data from flask api
  fetch('http://localhost:5000/topArtistPerCountry')
    .then(data => data.json())
    .then(data => {
      const items = data.items;
      for (var i = 0; i < items.length; i++) {
        var Country = items[i];
        const marker = L.marker(Country.location)
          .bindPopup("<h1>" + Country.name + "</h1> <hr> <h3>Most Streamed Song is " + Country.MostStreamedArtist + "</h3>")
          .addTo(myMap);
        currentMarkers.push(marker);
      }
    })
    .catch(e => console.log('error fetching data:', e));
}

function showTopitemsPerCountry(){
  // fetch data from flask api
  fetch('http://localhost:5000/topSongPerCountry')
    .then(data => data.json())
    .then(data => {
      const items = data.items;
      for (var i = 0; i < items.length; i++) {
        var Country = items[i];
        L.marker(Country.location)
          .bindPopup("<h1>" + Country.name + "</h1> <hr> <h3>Most Streamed Song is " + Country.MostStreamedArtist + "</h3>")
          .addTo(myMap);
      }
    })
    .catch(e => console.log('error fetching data:', e));
}