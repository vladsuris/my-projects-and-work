from flask import Flask
import sqlite3
import json

app = Flask(__name__)

country_code_to_latlong = { }
with open('./country_codes.json') as json_file:
    country_code_to_latlong = json.load(json_file)

def get_lat_long_from_country(country):
    if country in country_code_to_latlong:
        return country_code_to_latlong[country]
    return None

def query_db(query_text):
    conn = sqlite3.connect("spotify.db")
    conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    cur.execute(query_text)
    rows = cur.fetchall()
    rows = [ dict(zip(row.keys(), row)) for row in rows ]
    return rows

def query_db_and_add_location(query_text):
    # fetch data from db
    rows = query_db(query_text)
    
    # add the location for each row because it is not in the sql data
    rows = [ {
        **row, 
        "Location": get_lat_long_from_country(row['Country'])
    } for row in rows ]

    return rows

@app.route("/topSongPerCountryFromDb")
def topSongPerCountryFromDb():
    rows = query_db_and_add_location("SELECT * FROM spotifydata WHERE Rank = '1'")

    return {
        "items": rows
    }


@app.route("/topSongPerCountry")
def gettopsong():
    return {
        "items": [{
            "location": [37.09024,	-95.712891],
            "name": "United States",
            "MostStreamedSong": "Heartless by The Weekend"
        },
        {
            "location": [-38.416097,	-63.616672],
            "name": "Argentina",
            "MostStreamedSong": "RITMO (Bad Boys For Life) by The Black Eyed Peas"
        },
        {
            "location": [47.516231,	14.550072],
            "name": "Austria",
            "MostStreamedSong": "Der Bratan bleibt der gleiche by Capital Bra"
        },
        {
            "location": [-25.274398, 133.775136],
            "name": "Australia",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        },
        {
            "location": [50.503887,	4.469936],
            "name": "Belgium",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        },
        {
            "location": [42.733883,	25.48583],
            "name": "Bulgaria",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        },
        {
            "location": [-16.290154,	-63.588653],
            "name": "Bolivia",
            "MostStreamedSong": "Tusa by KAROL G"
        },
        {
            "location": [-14.235004,	-51.92528],
            "name": "Brazil",
            "MostStreamedSong": "Combatchy (feat. MC Rebecca) by Anitta"
        },
        {
            "location": [56.130366,	-106.346771],
            "name": "Canada",
            "MostStreamedSong": "Heartless by The Weekend"
        },
        {
            "location": [46.818188,	8.227512],
            "name": "Switzerland",
            "MostStreamedSong": "Der Bratan bleibt der gleiche by Capital Bra"
        },
        {
            "location": [-35.675147,	-71.542969],
            "name": "Chile",
            "MostStreamedSong": "Tusa by KAROL G"
        },
        {
            "location": [4.570868,	-74.297333],
            "name": "Columbia",
            "MostStreamedSong": "Tusa by KAROL G"
        },
        {
            "location": [9.748917,	-83.753428],
            "name": "Costa Rica",
            "MostStreamedSong": "Tusa by KAROL G"
        },
        {
            "location": [49.817492, 15.472962],
            "name": "Czech Republic",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        },
        {
            "location": [51.165691,	10.451526],
            "name": "Germany",
            "MostStreamedSong": "Der Bratan bleibt der gleiche by Capital Bra"
        },
        {
            "location": [56.26392,	9.501785],
            "name": "Denmark",
            "MostStreamedSong": "All I Want for Christmas by Maria Carey"
        },
        {
            "location": [18.735693,	-70.162651],
            "name": "Dominican Republic",
            "MostStreamedSong": "Vete by Bad Bunny"
        },
        {
            "location": [-1.831239,	-78.183406],
            "name": "Equador",
            "MostStreamedSong": "Tusa by KAROL G"
        },
        {
            "location": [58.595272,	25.013607],
            "name": "Estonia",
            "MostStreamedSong": "everything I wanted by Billie Ellish"
        },
        {
            "location": [40.463667,	-3.74922],
            "name": "Spain",
            "MostStreamedSong": "Alocao (With Bad Gyal) by Omar Montes"
        },
        {
            "location": [61.92411,	25.748151],
            "name": "Finland",
            "MostStreamedSong": "Hei rakas by BEHM"
        },
        {
            "location": [46.227638,	2.213749],
            "name": "France",
            "MostStreamedSong": "Ne reviens pas by Gradur"
        },
        {
            "location": [55.378051,	-3.435973],
            "name": "United Kingdom",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        },
        {
            "location": [0.0, 0.0],
            "name": "Worldwide",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        },
        {
            "location": [39.074208,	21.824312],
            "name": "Greece",
            "MostStreamedSong": "Oxi Simera by LEX"
        },
        {
            "location": [15.783471,	-90.230759],
            "name": "Guatemala",
            "MostStreamedSong": "Tusa by KAROL G"
        },
        {
            "location": [22.396428,	114.109497],
            "name": "Hong Kong",
            "MostStreamedSong": "Memories by Maroon 5"
        },
        {
            "location": [15.199999,	-86.241905],
            "name": "Hondouras",
            "MostStreamedSong": "Vete by Bad Bunny"
        },
        {
            "location": [47.162494,	19.503304],
            "name": "Hungary",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        },
        {
            "location": [-0.789275,	113.921327],
            "name": "Indonesia",
            "MostStreamedSong": "Memories by Maroon 5"
        },
        {
            "location": [53.41291,	-8.24389],
            "name": "Ireland",
            "MostStreamedSong": "Before You Go by Lewis Capaldi"
        },
        {
            "location": [31.046051,	34.851612],
            "name": "Israel",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        },
        {
            "location": [20.593684,	78.96288	],
            "name": "India",
            "MostStreamedSong": "Filhall by B Praak"
        },
        {
            "location": [64.963051,	-19.020835],
            "name": "Iceland",
            "MostStreamedSong": "Þegar Þú Blikkar (feat. Björgvin Halldórsson) by Herra Hnetusmjör"
        },
        {
            "location": [41.87194,	12.56738],
            "name": "Italy",
            "MostStreamedSong": "blun7 a swishland by tha Supreme"
        },
        {
            "location": [36.204824,	138.252924],
            "name": "Japan",
            "MostStreamedSong": "Pretender by Official HIGE DANdism"
        },
        {
            "location": [55.169438,	23.881275],
            "name": "Lithuania",
            "MostStreamedSong": "everything I wanted by Billie Ellish"
        },
        {
            "location": [56.879635,	24.603189],
            "name": "Latvia",
            "MostStreamedSong": "Falling by Trevor Daniel"
        },
        {
            "location": [23.634501, -102.552784],
            "name": "Mexico",
            "MostStreamedSong": "Falling by Trevor Daniel"
        },
        {
            "location": [4.210484,	101.975766],
            "name": "Malaysia",
            "MostStreamedSong": "Memories by Maroon 5"
        },
        {
            "location": [12.865416,	-85.207229],
            "name": "Nicaragua",
            "MostStreamedSong": "Tusa by KAROL G"
        },
        {
            "location": [52.132633,	5.291266],
            "name": "Netherlands",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        },
        {
            "location": [60.472024,	8.468946],
            "name": "Norway",
            "MostStreamedSong": "All I Want for Christmas by Maria Carey"
        },
        {
            "location": [-40.900557,	174.885971],
            "name": "New Zealand",
            "MostStreamedSong": "ROXANNE by Arizona Zervas"
        },
        {
            "location": [8.537981,	-80.782127],
            "name": "Panama",
            "MostStreamedSong": "Tusa by KAROL G"
        },
        {
            "location": [-9.189967,	-75.015152],
            "name": "Peru",
            "MostStreamedSong": "Tusa by KAROL G"
        },
        {
            "location": [12.879721,	121.774017],
            "name": "Philippines",
            "MostStreamedSong": "Memories by Maroon 5"
        },
        {
            "location": [51.919438,	19.145136],
            "name": "Poland",
            "MostStreamedSong": "Jesteś ładniejsza niż na zdjęciach (na zawsze) by Bedoes"
        },
        {
            "location": [39.399872, -8.224454],
            "name": "Portugal",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        },
        {
            "location": [-23.442503,	-58.443832],
            "name": "Paraguay",
            "MostStreamedSong": "Fantasias by Rauw Alejandro"
        },
        {
            "location": [45.943161,	24.96676],
            "name": "Romania",
            "MostStreamedSong": "Heartless by The Weekend"
        },
        {
            "location": [60.128161,	18.643501],
            "name": "Sweden",
            "MostStreamedSong": "All I Want for Christmas by Maria Carey"
        },
        {
            "location": [1.352083,	103.819836],
            "name": "Singapore",
            "MostStreamedSong": "Memories by Maroon 5"
        },
        {
            "location": [48.669026,	19.699024],
            "name": "Slovakia",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        },
        {
            "location": [13.794185,	-88.89653],
            "name": "El Salvador",
            "MostStreamedSong": "Tusa by KAROL G"
        },
        {
            "location": [15.870032,	100.992541],
            "name": "Thailand",
            "MostStreamedSong": "แอบดี by Stamp"
        },
        {
            "location": [38.963745,	35.243322],
            "name": "Turkey",
            "MostStreamedSong": "Toz Taneleri by Sagopa Kajmer"
        },
        {
            "location": [-32.522779,	-55.765835],
            "name": "Uruguay",
            "MostStreamedSong": "RITMO (Bad Boys For Life) by The Black Eyed Peas"
        },
        {
            "location": [23.69781, 120.960515],
            "name": "Taiwan",
            "MostStreamedSong": "Without You by 高爾宣 OSN"
        },
        {
            "location": [14.058324,	108.277199],
            "name": "Vietnam",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        },
        {
            "location": [	-30.559482,	22.937506],
            "name": "South Africa",
            "MostStreamedSong": "Dance Monkey by Tones and I"
        }
    ]}

@app.route("/topArtistPerCountry")
def gettopartist():
    return {
        "items": [{
            "location": [37.09024,	-95.712891],
            "name": "United States",
            "MostStreamedArtist": "Post Malone with:<hr> 5,041,435 Streams"
        },
        {
            "location": [-38.416097,	-63.616672],
            "name": "Argentina",
            "MostStreamedArtist": "Dalex with:<hr> 654,614 Streams"
        },
        {
            "location": [47.516231,	14.550072],
            "name": "Austria",
            "MostStreamedArtist": "Captial Bra with:<hr> 148,970 Streams"
        },
        {
            "location": [-25.274398, 133.775136],
            "name": "Australia",
            "MostStreamedArtist": "Post Malone with:<hr> 554,904 Streams"
        },
        {
            "location": [50.503887,	4.469936],
            "name": "Belgium",
            "MostStreamedArtist": "Billie Eilish with<hr> 119,967 Streams"
        },
        {
            "location": [42.733883,	25.48583],
            "name": "Bulgaria",
            "MostStreamedArtist": "Billie Eilish with<hr> 6,520 Streams"
        },
        {
            "location": [-16.290154,	-63.588653],
            "name": "Bolivia",
            "MostStreamedArtist": "Piso 21 with:<hr> 39,037 Streams"
        },
        {
            "location": [-14.235004,	-51.92528],
            "name": "Brazil",
            "MostStreamedArtist": "Marília Mendonça with:<hr> 3,025,122 Streams"
        },
        {
            "location": [56.130366,	-106.346771],
            "name": "Canada",
            "MostStreamedArtist": "Post Malone with:<hr> 697,835 Streams"
        },
        {
            "location": [46.818188,	8.227512],
            "name": "Switzerland",
            "MostStreamedArtist": "Capital Bra with:<hr> 143,615 Streams"
        },
        {
            "location": [-35.675147,	-71.542969],
            "name": "Chile",
            "MostStreamedArtist": "Dalex with:<hr> 727,074 Streams"
        },
        {
            "location": [4.570868,	-74.297333],
            "name": "Colombia",
            "MostStreamedArtist": "J Balvin with:<hr> 210,824 Streams"
        },
        {
            "location": [9.748917,	-83.753428],
            "name": "Costa Rica",
            "MostStreamedArtist": "Sech with:<hr> 66,069 Streams"
        },
        {
            "location": [49.817492, 15.472962],
            "name": "Czech Republic",
            "MostStreamedArtist": "Renne Dang with:<hr> 101,528 Streams"
        },
        {
            "location": [51.165691,	10.451526],
            "name": "Germany",
            "MostStreamedArtist": "Apache 207 with:<hr> 3,454,802 Streams"
        },
        {
            "location": [56.26392,	9.501785],
            "name": "Denmark",
            "MostStreamedArtist": "ATYPISK with:<hr> 172,995 Streams"
        },
        {
            "location": [18.735693,	-70.162651],
            "name": "Dominican Republic",
            "MostStreamedArtist": "Ozuna with:<hr> 134,320 Streams"
        },
        {
            "location": [-1.831239,	-78.183406],
            "name": "Equador",
            "MostStreamedArtist": "J Balvin with:<hr> 82,904 Streams"
        },
        {
            "location": [58.595272,	25.013607],
            "name": "Estonia",
            "MostStreamedArtist": "Billie Eilish with<hr> 9,360 Streams"
        },
        {
            "location": [40.463667,	-3.74922],
            "name": "Spain",
            "MostStreamedArtist": "Beret with:<hr> 709,238 Streams"
        },
        {
            "location": [61.92411,	25.748151],
            "name": "Finland",
            "MostStreamedArtist": "Pyhimys with:<hr> 126,663 Streams"
        },
        {
            "location": [46.227638,	2.213749],
            "name": "France",
            "MostStreamedArtist": "SCH with:<hr> 2,926,854 Streams"
        },
        {
            "location": [55.378051,	-3.435973],
            "name": "United Kingdom",
            "MostStreamedArtist": "Lewis Capaldi with: 1,115,661 Streams"
        },
        {
            "location": [0.0, 0.0],
            "name": "Worldwide",
            "MostStreamedArtist": "Post Malone with: 11,666,859 Streams"
        },
        {
            "location": [39.074208,	21.824312],
            "name": "Greece",
            "MostStreamedArtist": "TOQUEL with:<hr> 51,585 Streams"
        },
        {
            "location": [15.783471,	-90.230759],
            "name": "Guatemala",
            "MostStreamedArtist": "Bad Bunny with:<hr> 39,226 Streams"
        },
        {
            "location": [22.396428,	114.109497],
            "name": "Hong Kong",
            "MostStreamedArtist": "Ed Sheeran with:<hr> 32,346 Streams"
        },
        {
            "location": [15.199999,	-86.241905],
            "name": "Hondouras",
            "MostStreamedArtist": "Bad Bunny with:<hr> 39,226 Streams"
        },
        {
            "location": [47.162494,	19.503304],
            "name": "Hungary",
            "MostStreamedArtist": "Manuel with:<hr> 57,635 Streams"
        },
        {
            "location": [-0.789275,	113.921327],
            "name": "Indonesia",
            "MostStreamedArtist": "Andmesh with:<hr> 654,560 Streams"
        },
        {
            "location": [53.41291,	-8.24389],
            "name": "Ireland",
            "MostStreamedArtist": "Lewis Capaldi with:<hr> 183,206 Streams"
        },
        {
            "location": [31.046051,	34.851612],
            "name": "Israel",
            "MostStreamedArtist": "Eden Hason with:<hr> 40,276 Streams"
        },
        {
            "location": [20.593684,	78.96288	],
            "name": "India",
            "MostStreamedArtist": "Arijit Singh with: 704,825 Streams"
        },
        {
            "location": [64.963051,	-19.020835],
            "name": "Iceland",
            "MostStreamedArtist": "Herra Hnetusmjör with:<hr> 22,326 Streams"
        },
        {
            "location": [41.87194,	12.56738],
            "name": "Italy",
            "MostStreamedArtist": "tha Supreme with<hr> 4,216,282 Streams"
        },
        {
            "location": [36.204824,	138.252924],
            "name": "Japan",
            "MostStreamedArtist": "Official HIGE DANdism with:<hr> 838,658 Streams"
        },
        {
            "location": [55.169438,	23.881275],
            "name": "Lithuania",
            "MostStreamedArtist": "Billie Eilish with:<hr> 12,881 Streams"
        },
        {
            "location": [56.879635,	24.603189],
            "name": "Latvia",
            "MostStreamedArtist": "Billie Eilish with:<hr> 8,903 Streams"
        },
        {
            "location": [23.634501, -102.552784],
            "name": "Mexico",
            "MostStreamedArtist": "Bad Bunny with:<hr> 17,652 Streams"
        },
        {
            "location": [4.210484,	101.975766],
            "name": "Malaysia",
            "MostStreamedArtist": "Lauv with:<hr> 102,766 Streams"
        },
        {
            "location": [12.865416,	-85.207229],
            "name": "Nicaragua",
            "MostStreamedArtist": "Bad Bunny with:<hr> 17,652 Streams"
        },
        {
            "location": [52.132633,	5.291266],
            "name": "Netherlands",
            "MostStreamedArtist": "Sven Alias with:<hr> 757,473 Streams"
        },
        {
            "location": [60.472024,	8.468946],
            "name": "Norway",
            "MostStreamedArtist": "Kurt Nilsen with:<hr> 315,315 Streams"
        },
        {
            "location": [-40.900557,	174.885971],
            "name": "New Zealand",
            "MostStreamedArtist": "SIX60 with:<hr> 278,985 Streams"
        },
        {
            "location": [8.537981,	-80.782127],
            "name": "Panama",
            "MostStreamedArtist": "Ozuna with:<hr> 64,613 Streams"
        },
        {
            "location": [-9.189967,	-75.015152],
            "name": "Peru",
            "MostStreamedArtist": "Dales with:<hr> 252,555 Streams"
        },
        {
            "location": [12.879721,	121.774017],
            "name": "Philippines",
            "MostStreamedArtist": "Ben&Ben with: 624,849 Streams"
        },
        {
            "location": [51.919438,	19.145136],
            "name": "Poland",
            "MostStreamedArtist": "Bedoes with:<hr> 1,601,691 Streams"
        },
        {
            "location": [39.399872, -8.224454],
            "name": "Portugal",
            "MostStreamedArtist": "Plutónio with:<hr> 135,000 Streams"
        },
        {
            "location": [-23.442503,	-58.443832],
            "name": "Paraguay",
            "MostStreamedArtist": "Dalex with:<hr> 129,392 Streams"
        },
        {
            "location": [45.943161,	24.96676],
            "name": "Romania",
            "MostStreamedArtist": "Ian with<hr> 44,387 Streams"
        },
        {
            "location": [60.128161,	18.643501],
            "name": "Sweden",
            "MostStreamedArtist": "Miss Li with:<hr> 495,553 Streams"
        },
        {
            "location": [1.352083,	103.819836],
            "name": "Singapore",
            "MostStreamedArtist": "Luav with:<hr> 110,828 Streams"
        },
        {
            "location": [48.669026,	19.699024],
            "name": "Slovakia",
            "MostStreamedArtist": "Miss Li with:<hr> 495,553 Streams"
        },
        {
            "location": [13.794185,	-88.89653],
            "name": "El Salvador",
            "MostStreamedArtist": "Bad Bunny with:<hr> 37,227 Streams"
        },
        {
            "location": [15.870032,	100.992541],
            "name": "Thailand",
            "MostStreamedArtist": "Wanyai with:<hr> 105,971 Streams"
        },
        {
            "location": [38.963745,	35.243322],
            "name": "Turkey",
            "MostStreamedArtist": "Sagopa Kajmer with:<hr> 1,074,077 Streams"
        },
        {
            "location": [-32.522779,	-55.765835],
            "name": "Uruguay",
            "MostStreamedArtist": "Dalex with:<hr> 41,421 Streams"
        },
        {
            "location": [23.69781, 120.960515],
            "name": "Taiwan",
            "MostStreamedArtist": "G.E.M. with:<hr> 106,808 Streams"
        },
        {
            "location": [14.058324,	108.277199],
            "name": "Vietnam",
            "MostStreamedArtist": "Đen with:<hr> 70,447 Streams "
        },
        {
            "location": [	-30.559482,	22.937506],
            "name": "South Africa",
            "MostStreamedArtist": "Post Malone with:<hr> 32,091 Streams"
        }
    ]}


@app.route("/topStreamArtist")
def gettopstreamartist():
    return {
        "items": [{
            "location": [37.09024,	-95.712891],
            "name": "United States",
            "MostStreamedArtist": "Heartless by The Weekend"
        },
        {
            "location": [-38.416097, -63.616672],
            "name": "Argentina",
            "MostStreamedArtist": "RITMO (Bad Boys For Life) by The Black Eyed Peas"
        }]
    }

if __name__ == "__main__":
    app.run(debug=True)